# AskAvalon API
A rewrite of the AskSteem API for the Avalon blockchain

## Endpoints

### /search

Returns the results that match the provided query.

#### Parameters

* **q** - pass a query (can use Apache Lucene syntax)
* **order** - how to order the results (asc or desc)
* **sort_by** - the field to sort by
* **start** - the starting index of results (default: 0)
* **end** - the ending index of results (default: 10)
* **index** - the Elasticsearch index that should be searched (by default all indexes)

### /suggestions

Returns query suggestions that can be displayed to the user while they are typing.

#### Parameters

- **term** - the query you want suggestions for



# Installation

```bash
virtualenv env --python python3
source env/bin/activate
pip install -r requirements.txt
# To run
python application.py
```

If needed you can change the Elasticsearch host configuration in the **settings.json** file. 