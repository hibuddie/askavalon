﻿# Flask
from flask import Response

# General
import json

# Endpoints
from .search import search
from .suggestions import suggestions
from .related import related
from .trending import trending

def produce_response_from_json(json):
	return Response(
		json,
		content_type='application/json'
	)


'''
This is a utility function used to 
easily call endpoints from within
other parts of the codebase.

Takes:
- endpoint (str)
- params (dict)
'''
def call_endpoint(endpoint, params):
	if endpoint == 'search':
		return produce_response_from_json(search(params))
	elif endpoint == 'suggestions':
		return produce_response_from_json(suggestions(params))
	elif endpoint == 'related':
		return produce_response_from_json(related(params))
	elif endpoint == 'trending':
		return produce_response_from_json(trending(params))
	else:
		return produce_response_from_json(json.dumps({
			'error': True,
			'message': 'invalid endpoint'
		}))