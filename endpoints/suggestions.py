﻿# General
import json
import requests

def suggestions(params):
	# get query
	query = params.get('term', default='')
	# pass query to google
	queryUrl = 'https://suggestqueries.google.com/complete/search?client=firefox&q=' + query
	suggestionsList = json.loads((requests.get(queryUrl).text))[1][:5]

	return json.dumps(suggestionsList)
