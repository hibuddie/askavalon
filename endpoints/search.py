﻿from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
import json

# Load Settings
settings = json.load(open('settings.json'))

# Create Elasticsearch Client
client = Elasticsearch(
	settings['ELASTICSEARCH_ENDPOINT'],
	http_auth=(
		settings['ELATICSEARCH_AUTH_USERNAME'], 
		settings['ELATICSEARCH_AUTH_PASSWORD']
	),
	timeout=500, 
	max_retries=10, 
	retry_on_timeout=True
)

def search(params):
	s = Search(using=client, index=params.get('index', '')) \
		.query("simple_query_string", query=params.get('q', '')) \
		.sort({params.get('sort_by', '_score'): {"order" : params.get('order', 'desc')}})

	start = int(params.get('start', 0))
	end = int(params.get('end', 10))
	response = s[start:end].execute()

	return json.dumps({
		'hits': response.hits.total,
		'time': response.took / 1000.0,
		'results': [h.to_dict() for h in response.hits]
	})