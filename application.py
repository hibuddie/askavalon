﻿# Flask
from flask import Flask
from flask import Response
from flask import request
# CORS
from flask_cors import CORS
# General
import json
# Call endpoint
from endpoints import call_endpoint

# Init app
application = Flask(__name__)
CORS(application)

@application.route('/')
def index():
	return Response(
		json.dumps({
			'error':True,
			'message':'Please use a API endpoint.'
		}),
		mimetype='application/json'
	)

@application.route('/search')
def search():
	return call_endpoint(
		'search', 
		request.args
	)

@application.route('/suggestions')
def suggestions():
	return call_endpoint(
		'suggestions', 
		request.args
	)

@application.route('/related')
def related():
	return call_endpoint(
		'related', 
		request.args
	)

@application.route('/trending')
def trending():
	return call_endpoint(
		'trending', 
		request.args
	)

if __name__ == "__main__":
	application.run()